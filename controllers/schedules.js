const Database = require('../utils/database');
const ScheduleDao = require('../daos/scheduleDao');
const ScheduleByRangeBuilder = require('../utils/schedulesByRangeBuilder');

module.exports = app => {
  const dao = new ScheduleDao(Database.getConnection());

  const SchedulesController = {
    listAll: async (req, res) => {
      try {
        const list = await dao.findAll() || [];
        res.json(list);
      } catch(err) {
        console.log(err);
        res.status(500).json({msg: 'Server internal error'});
      }
    },
    create: async (req, res) => {
      try {
        const schedule = await dao.save(req.body);
        res.location(`/api/v1/schedules/${schedule.id}`);
        res.status(201).json(schedule);
      } catch(err) {
        console.log(err);
        res.status(500).json({msg: 'Server internal error'});
      }
    },
    delete: async (req, res) => {
      try {
        const id = req.params.id;
        const result = await dao.remove(id);
        const success = result.length;
        if(!success) {
          res.sendStatus(400);
        } else {
          res.sendStatus(204);
        }
      } catch(err) {
        console.log(err);
        res.status(500).json({msg: 'Server internal error'});
      }
    },
    getAvailableTimes: async (req, res) => {
      try {
        const list = await dao.findAll() || [];
        const { from, to } = req.query;

        res.json(ScheduleByRangeBuilder.getAvailableTimes(list, from, to));
      } catch(err) {
        console.log(err);
        res.status(500).json({msg: 'Server internal error'});
      }
    }
  };

  return SchedulesController;
};
