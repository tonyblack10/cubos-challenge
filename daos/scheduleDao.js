class ScheduleDao {
  constructor(connection) {
    this._connection = connection;
  }

  findAll() {
    return this._connection
      .then(db => 
        db.get('schedules')
          .value());
  }

  save(schedule) {
    return this._connection
      .then(db => 
        db.get('schedules')
          .push(schedule)
          .last()
          .assign({ id: Date.now().toString() })
          .write());
  }

  remove(scheduleId) {
    return this._connection
      .then(db => 
        db.get('schedules')
          .remove({id: scheduleId})
          .write());
  }
}

module.exports = ScheduleDao;
