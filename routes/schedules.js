const validateIntervals = require('../middlewares/validateIntervals');

module.exports = app => {
  const { schedules } = app.controllers;
  
  app.route('/api/v1/schedules')
    .get(schedules.listAll)
    .post(validateIntervals, schedules.create);

  app.delete('/api/v1/schedules/:id', schedules.delete);

  app.get('/api/v1/schedules/available', schedules.getAvailableTimes);

};
