const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);

const _filterByRange = (list, from, to) => {
  return list.filter(item => {
    const dateItem = moment(item.day, 'DD-MM-YYYY');
    const dateFrom = moment(from, 'DD-MM-YYYY');
    const dateTo = moment(to, 'DD-MM-YYYY');

    const range = moment().range(dateFrom, dateTo);

    return range.contains(dateItem);
  });
};

const _getDailySchedules = (list, from, to) => {
  const result = [];
  const dateFrom = moment(from, 'DD-MM-YYYY');
  const dateTo = moment(to, 'DD-MM-YYYY');
  const range = moment.range(dateFrom, dateTo);
  const daylyIntervals = list.filter(item => item.type == 'DAYLY')
    .map(item => item.intervals)
    .reduce((result, item) => item.concat(result), []);

  for (let day of range.by('day')) {
    let weeklySchedules = _getWeeklySchedules(list, day);
    if(weeklySchedules.length > 0) {
      result.push({day: day.format('DD-MM-YYYY'), intervals: weeklySchedules.concat(daylyIntervals)});
    } else {
      result.push({day: day.format('DD-MM-YYYY'), intervals: daylyIntervals});
    }
  }

  return result
};

const _getWeeklySchedules = (list, day) => {
  return list.filter(item => item.type == 'WEEKLY' && day.weekday() == item.dayOfWeek)
    .map(item => item.intervals)
    .reduce((result, item) => item.concat(result), []);
};

const _groupByDay = list => list.reduce((groups, item) => {
  const value = item['day'];
  
  if(groups[value]) {
    groups[value].push(item.intervals);
  } else {
    groups[value] = [];
    groups[value].push(item.intervals);
  }

  return groups;
}, {});

const getAvailableTimes = (list, from, to) => {
  const rangeFilteredByDay = _groupByDay(_filterByRange(list, from, to)
    .concat(_getDailySchedules(list, from, to)));

  const result = Object.keys(rangeFilteredByDay)
    .sort((a, b) => {
      if(moment(a, 'DD-MM-YYYY').isBefore(moment(b, 'DD-MM-YYYY'))) {
        return -1;
      } if(moment(a, 'DD-MM-YYYY').isAfter(moment(b, 'DD-MM-YYYY'))) {
        return 1;
      } 
      return 0;
    })
    .map(value => {
      const sortedHours = rangeFilteredByDay[value]
        .reduce((reducer, item) => reducer.concat(item), [])
        .sort((a, b) => moment(a.start, 'HH:mm').isAfter(moment(b.start, 'HH:mm')));
      
      return {day: value, intervals: sortedHours};
    });

  result.map(r => r.intervals = _generateAvailableRanges(r.intervals));

  return result;
};

const _generateAvailableRanges = intervals => {
  if(intervals.length === 0) {
    return [{start: '07:00', end: '20:00'}];
  }
  const availableIntervals = [];
  intervals.forEach((item, index, arr) => {
    if(index === 0 && item.start != '07:00') {
      availableIntervals.push({start: '07:00', end: item.start});
    } if(index < arr.length-1) {
      availableIntervals.push({start: item.end, end: arr[index+1].start});
    } if(index == arr.length-1 && item.end != '20:00') {
      availableIntervals.push({start: item.end, end: '20:00'});
    }
  });
  
  return availableIntervals;
}; 


module.exports = { getAvailableTimes };
