const low = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');
const adapter = new FileAsync('db.json');

const Database = {
  initialize: () => 
    low(adapter)
      .then(db => db.defaults({ schedules: [] }).write())
  ,
  getConnection: () => low(adapter)
};

module.exports = Database;
