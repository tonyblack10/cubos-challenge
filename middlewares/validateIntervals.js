const timeValidators = require('../validators/timeValidators');

module.exports = async (req, res, next) => {
  const schedule = req.body;

  if(schedule.type == 'DAYLY') {
    const isValid = await timeValidators.isValidDaylySchedule(schedule);
    if(!isValid) {
      res.status(400).json({message: 'Invalid time intervals'});
      return;
    }
  } else if(schedule.type == 'WEEKLY') {
    const isValid = await timeValidators.isValidWeeklySchedule(schedule);
    if(!isValid) {
      res.status(400).json({message: 'Invalid time intervals'});
      return;
    }
  } else {
    const isValid = await timeValidators.isValidDaySchedule(schedule);
    if(!isValid) {
      res.status(400).json({message: 'Invalid time intervals'});
      return;
    }
  }

  next();
};
