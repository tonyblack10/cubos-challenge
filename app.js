const express = require('express');
const bodyParser = require('body-parser');
const consign = require('consign');

const Database = require('./utils/database');

// Create server
const app = express();
app.use(bodyParser.json());
app.use(express.static('public'));

consign()
  .include('controllers')
  .then('routes')
  .into(app);

Database.initialize()
  .then(() => 
    app.listen(3000, () => console.log('listening on port 3000'))
  );
