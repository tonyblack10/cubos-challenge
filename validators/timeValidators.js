const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);

const Database = require('../utils/database');
const ScheduleDao = require('../daos/scheduleDao');

const dao = new ScheduleDao(Database.getConnection());

const isValidDaylySchedule = async (schedule) => {
  const list = await dao.findAll();
  const intervals = list.map(item => item.intervals)
    .reduce((reducer, item) => reducer.concat(item), [])
    .map(item => {
      return {start: moment(item.start, 'HH:mm'), end: moment(item.end, 'HH:mm')};
    });

  return _isValidRange(schedule, intervals);
};

const isValidWeeklySchedule = async (schedule) => {
  const list = await dao.findAll();
  const intervals = list
    .filter(item => item.type === 'DAYLY' ||
      (item.type === 'WEEKLY' && item.dayOfWeek == schedule.dayOfWeek) || 
      (item.type === 'DAY' && moment(item.day, 'DD-MM-YYYY').weekday() == schedule.dayOfWeek))
    .map(item => item.intervals)
    .reduce((reducer, item) => reducer.concat(item), [])
    .map(item => {
      return {start: moment(item.start, 'HH:mm'), end: moment(item.end, 'HH:mm')};
    });

    return _isValidRange(schedule, intervals);
};

const isValidDaySchedule = async (schedule) => {
  const list = await dao.findAll();
  const intervals = list
    .filter(item => item.type === 'DAYLY' ||
      (item.type === 'WEEKLY' && item.dayOfWeek == schedule.dayOfWeek) || 
      (item.type === 'DAY' && item.day == schedule.day) || 
      (item.type === 'WEEKLY' && item.dayOfWeek == moment(schedule.day, 'DD-MM-YYYY').weekday()))
    .map(item => item.intervals)
    .reduce((reducer, item) => reducer.concat(item), [])
    .map(item => {
      return {start: moment(item.start, 'HH:mm'), end: moment(item.end, 'HH:mm')};
    });

    return _isValidRange(schedule, intervals);
};

const _isValidRange = (schedule, intervals) => {
  const scheduleIntervalsMoment = schedule.intervals.map(item => {
    return {start: moment(item.start, 'HH:mm'), end: moment(item.end, 'HH:mm')};
  });

  return !intervals.some(item => {
    const range = moment.range(item.start, item.end);
    return scheduleIntervalsMoment.some(i => 
      range.isEqual(moment.range(i.start, i.end)) ||
      range.contains(i.start, {excludeStart: true, excludeEnd: true})
    );
  });
};


module.exports = { isValidDaylySchedule, isValidWeeklySchedule, isValidDaySchedule };
